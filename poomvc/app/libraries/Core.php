<?php

class Core
{

    protected $controladorActual = "Paginas"; //Controlador por defecto

    protected $metodoActual = "index"; // Método por defecto

    protected $parametros = []; // Por defecto no hay parámetros

    
   

    public function __construct()
    {
        
        

        // obtenemos  la URL en forma de array 
        $url = $this->getUrl();

        // si en la posicion cero de la url existe un nombre de controlador 
        // igual al nombre del contolador que está dentro de la carpeta controllers
        // pasará a ser nuestro controlador
        //

        if (file_exists('../app/controllers/' . ucwords($url[0]) .'.php')) {
            // devuelve una palabra con la primera letra en mayúscula    
            $this->controladorActual = ucwords($url[0]);
           
            // eliminamos la variable $url
            unset($url[0]);
        }

        // traemos el controlador cuyo nombre venia en la url
        require_once '../app/controllers/' . $this->controladorActual . '.php';
        // instanciamos nuestro controlador actual
        $this->controladorActual = new $this->controladorActual;
        $this->controladorActual;

        // si esta seteado la posición dos de array $url
        if (isset($url[1])) {
            // comprueba si existe  el método de la clase 
            if (method_exists($this->controladorActual, $url[1])) {
                //si existe el nuevo método pasa a ser el de la variable $url[1]
                $this->metodoActual = $url[1];
                // elimina la posicion 2 de $url
                unset($url[1]);
            }
        }
        // echo $this->metodoActual;

        // Llama a la llamada de retorno dada por [$this->controladorActual, $this->metodoActual]
        // con los parámetros de $this->parametros.
        $this->parametros = $url ? array_values($url) : [];
        call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros);
    }






    public function getUrl()
    {

        if (isset($_GET['url'])) {

            // eliminamos la barra invertida al final de la url
            $url = rtrim($_GET['url'], '/');
            // Elimina todos los caracteres 
            //excepto letras, dígitos y $-_.+!*'(),{}|\\^~[]`<>#%";/?:@&=.
            $url = filter_var($url, FILTER_SANITIZE_URL);
            // nos devuelve un array de string siendo cada uno 
            // un substring del parámetro string formado por la división 
            // realizada por /.
            $url = explode('/', $url);
         
        } else {

            $url[0] = 'Paginas';
        }
     

        return $url;
    }




}
?>