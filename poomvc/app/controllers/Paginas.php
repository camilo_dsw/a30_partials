<?php

//require_once '../app/bootstrap.php';
require_once '../app/libraries/Controller.php';

class Paginas extends Controller
{
    public function __construct()
    {

        echo 'Soy el controlador por defecto Paginas';
    }


    public function index()
    {        
        $data = array('titulo' => 'FrameWork de Fran');

        $this->view('paginas/index',$data);
    }

    public function about()
    {
        $this-> view('paginas/about');
    }


    
    public function actualizar($id)
    {
        echo "Método actualizar\n";
        echo $id;
    }
   
}

//$b = new Paginas();
//$b->index();
//$b->about();
?>