<?php

class Controller
{

    public function model($modelo)
    { 
        require_once '/var/www/html/A30_partials/poomvc/app/models/'.$modelo.'.php';
        return new $modelo;
        
    }

    // Si el fichero existe lo carga, en caso contrario informa del error y muere
    public function view($vista,$datos = [])
    {
        if (file_exists('../app/views/' . $vista . '.php')) {

            require_once '../app/views/' . $vista . '.php';
        } else {
            echo "</br>";  
            die('El fichero no existe ');
        }
    }
    
}
?>